select T.MARKET_ABBR, T.DH_DRAW_RATECODE_W, T.PRODUCT_S, Y.market_abbr, Y.rate_code_s, y.product_s, y.date_rate_start, y.rate_status,
	t.HDATE_YEAR_NUM, t.HDATE_MONTH_NUM, t.DH_DAY_NUM, t.rowid
from edw_si_rss.si_cir_history_day T
	JOIN edw_si_rss.DIM_DATE D ON  T.dim_date_history_key = D.dim_date_key
	join (select distinct * from YC_TEMP_RATE_ANALYSIS)  Y  --(select distinct * from YC_TEMP_RATE_ANALYSIS)
 on Y.market_abbr = t.MARKET_ABBR
   and Y.rate_code_S = t.DH_DRAW_RATECODE_W
   and y.product_s = t.PRODUCT_S
   and Y.date_rate_start <=  d.day_date   --to_date( D.DAY_NAME,'dd-Mon-yyyy')
   and Y.date_rate_start IN 
   (
   	select MAX( ytop.date_rate_start) from (select distinct * from YC_TEMP_RATE_ANALYSIS) ytop
   	 where ytop.market_abbr = y.market_abbr 
   	   and ytop.rate_code_s = y.rate_code_s
   	   and ytop.rate_status = 'A'
   	   and ytop.product_s = y.product_s
   ) 
 where t.market_abbr = 'CAL'
   and t.DH_DRAW_RATECODE_W = '942'
   and t.PRODUCT_S = '1'
;



select * from YC_TEMP_RATE_ANALYSIS Y
 where rate_code_S = '942'
order by market_abbr, rate_code_s, product_s, date_rate_start DESC
;


select   market_abbr, rate_code_s, product_s, count(date_rate_start)
from     YC_TEMP_RATE_ANALYSIS Y
group by market_abbr, rate_code_s, product_s
having   count(date_rate_start)>1
order by market_abbr, rate_code_s, product_s

;

select * from edw_si_rss.DIM_DATE D 
where rownum < 10
and day_name is not NULL
and day_date >= trunc(sysdate-20)