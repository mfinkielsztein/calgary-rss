-- TASK0420695: 27-Nov-2015.  Add rates.
--
--file name: Dealers_2015_07_06_(weekly).xlsx
--                        mm dd
--save it to folder: \\calhldfil01\RSM\SingleCopy\Reporting\
--
--
--select max(hdate_year_num) FROM edw_si_rss.si_cir_history_day T
--where  
--	T.Market_Abbr = 'CAL' 
--	--and Prod.MARKET_ABBR = 'CAL' and R.MARKET_ABBR = 'CAL' and DLR.MARKET_ABBR = 'CAL'
--	and dh_carrier_s like 'D%'  
--	--and dlr.CARRIER_ACCT_TYPE_DESC = 'DEALER'
--	and t.dh_rtemas_carrier_status_s = 'A'   -- Active
--	and t.product_s in ( '1', '4')   -- 1=Calgary Herald; 2=National Post 
----	  and hdate_year_num  IN (2015)
--	  and hdate_month_num IN ( 01 )  --1=JAN, 2=FEB, 3=MAR, 4=APR, 5=MAY, 6=JUN, 7=JUL, 8=AUG, 9=SET, 10=OCT, 11=NOV, 12=DEC
--	  and 
--	  (
--		t.route_s like '057%'
--	  	OR t.route_s like '857%'
--       )
--
--;
--
--select 
--	DrawYear, DrawMonth, MAX(cal_month_name) as MonthName, Classification, MAX(Classification_name) as Classification_name
--		, count(*) as Draw_Count, SUM(TotalDraw) as SumTotalDraw, SUM(Returns) as SumReturns, SUM(sellout) as SellOut
--		, COUNT(Distinct Dealer) as Distinct_Dealer_Count, COUNT(Distinct Route) as Distinct_Route_Count 	
-- from
-- (  
--  --------------
--  -- DETAIL   --
--  --------------
--
--
	SELECT   
		t.hdate_year_num as DrawYear, t.hdate_month_num as DrawMonth, t.dh_day_num as DrawDay
		, t.dh_carrier_s as Dealer, DLR.CARRIER_FIRST_NAME as DealerFirstName, DLR.CARRIER_LAST_NAME as DealerLastName
		, DLR.BILLING_CLASS_DESC, DLR.BILLING_SUBCLASS_DESC, DLR.AREA_CODE, DLR.PHONE_NUMBER
		, DLR.CARRIER_STATUS_DESC
		, t.route_s as Route, R.SUN_ID_FLAG8 as SUN_ID
		--
		, substr( t.route_s, 1, 3) as Classification
		, case
			when substr( t.route_s, 1, 3)='057' then 'CH-City'
			when substr( t.route_s, 1, 3)='157' then 'NP-City'	          
			when substr( t.route_s, 1, 3)='857' then 'CH-Country' 
			when substr( t.route_s, 1, 3)='957' then 'NP-Country'
			else 'UNKNOWN'
		end as Classification_name
		--
		, t.market_abbr as market, t.product_s as Product, Prod.PRODUCT_DESC, t.EDITION_S as Edition
		, D.day_name as day_name, to_char( D.day_date, 'Day') as WeekDay, D.CAL_MONTH_NAME
		, T.dh_draw_w as Draw, t.dh_extras_w as Extras, t.dh_increase_w as TempIncrease
		,  (T.dh_draw_w  + t.dh_extras_w  + t.dh_increase_w) as TotalDraw
		, t.dh_returns_w as Returns, case when t.dh_returns_w=0 then 1 else 0 end as sellout
		, r.returns_allowed, r.bundle_type_desc
		, t.route_type_s, r.route_type_desc, r.route_status_desc
		--
		-- address begins
		, addr.CITY
		, addr.HOUSE_NUMBER, addr.HOUSE_MODIFIER, addr.UNIT_NUMBER, addr.STREET_NAME, addr.STREET_POST_DIRECTION, addr.PROVINCE, addr.COUNTRY, addr.POSTAL_CODE, addr.FSA
		, addr.ADDRESS_1, addr.ADDRESS_2, addr.ADDRESS_TYPE, addr.BUILDING, addr.UNIT_TYPE
		, addr.STREET_PRE_DIRECTION, addr.STREET_TYPE, addr.RURAL_ROUTE
		, addr.DWELLING_TYPE
		-- address ends
		--		
		--
		-- rate begins
		, y.rate_code_s as rate_code, y.rate_per_copy as rate_amount_per_copy
		-- rate end
		--
		--
	 FROM edw_si_rss.si_cir_history_day T
	 JOIN edw_si_rss.DIM_DATE D ON  T.dim_date_history_key = D.dim_date_key
      JOIN edw_si_rss.dim_cir_route_vms R ON T.dim_route_key = R.dim_route_key
      JOIN edw_si_rss.DIM_CIR_PRODUCT_VMS Prod ON T.product_s = Prod.PRODUCT_CD AND T.dim_product_key = Prod.Dim_Product_Key
      JOIN edw_si_rss.DIM_CIR_CARRIER_VMS Dlr ON T.DIM_CARRIER_KEY = Dlr.DIM_CARRIER_KEY
      JOIN edw_si_rss.DIM_CIR_CARRIER_ADDRESS_VMS Addr ON T.DIM_ADDRESS_KEY = addr.DIM_ADDRESS_KEY
      ---
      ---
		-- RATES BEGIN
		JOIN (select distinct * from EDW_SI_RSS.YC_RATE_ANALYSIS_TASK0420695) Y 
			ON t.MARKET_ABBR = Y.market_abbr
				and t.DH_DRAW_RATECODE_W = Y.rate_code_S
				and t.PRODUCT_S = y.product_s
				and Y.rate_status = 'A'
				and Y.date_rate_start <=  d.day_date
				and Y.date_rate_start IN 
				(
					select MAX( ytop.date_rate_start) from (select distinct * from EDW_SI_RSS.YC_RATE_ANALYSIS_TASK0420695) ytop
					 where ytop.market_abbr = y.market_abbr 
						and ytop.rate_code_s = y.rate_code_s
						and ytop.product_s = y.product_s
						and ytop.rate_status = 'A'
				) 
		-- RATES END
      ---	
      ---	
      ---	
  where  
		T.Market_Abbr = 'CAL' and Prod.MARKET_ABBR = 'CAL' and R.MARKET_ABBR = 'CAL' and DLR.MARKET_ABBR = 'CAL'
		and dh_carrier_s like 'D%'  and dlr.CARRIER_ACCT_TYPE_DESC = 'DEALER'
		and t.dh_rtemas_carrier_status_s = 'A'   -- Active
		and t.product_s in ( '1', '4')   -- 1=Calgary Herald; 2=National Post 
	--
	-----------------------------------------------
	-- PARAMETERS: CONFIGURATION SECTION BEGINS
	-----------------------------------------------
			--    Note:
			--        When results are saved into 
			--        \\calhldfil01\rsm\SingleCopy\Reporting\
			--        The CH team can go to SharePoint and retrieve them.
			--
		--
		--
		-- Monthly: 1=JAN, 2=FEB, 3=MAR, 4=APR, 5=MAY, 6=JUN, 7=JUL, 8=AUG, 9=SET, 10=OCT, 11=NOV, 12=DEC
		-- Weekly run.  Extracts rows from three weeks ago.
		--
			  AND 
			  	(		
					(
						--  Monthly Run begin
						upper( :Weekly_Monthly_WM )  = 'M' 
						 and t.hdate_year_num  IN (TO_NUMBER(TO_CHAR(ADD_MONTHS(trunc(SYSDATE, 'MONTH'),-2) ,'YYYY'))) 
						 and t.hdate_month_num IN (TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE, 'MONTH'),-2) ,'MM'  )))
						--  Monthly Run end						 
					) OR (
						-- between Three_Mondays_ago and Two_Sundays_Ago begin
						upper( :Weekly_Monthly_WM )  = 'W' and 
						D.DAY_DATE BETWEEN 
							NEXT_DAY( TRUNC(sysdate), 'MONDAY')-7-(3*7) AND 
							NEXT_DAY( TRUNC(sysdate), 'SUNDAY')-7-(2*7) 
						-- between Three_Mondays_ago and Two_Sundays_Ago end 
					)
				)
		--
		--	
       AND
       (
	     SUBSTR(t.route_s, 1, 3) IN 
		     (
		     	select '057' as classif from dual        --057 => herald, city
		     	union select '157' as classif from dual  --157 => nat post, city
		     	union select '857' as classif from dual  --857 => calg herald, rural (country)
		     	union select '957' as classif from dual  --957 => nat post, rural (country)
		     ) 
       )
	--   
	--  +---------------------------------------------------+
	--  | INCLUDE BOTH:  and r.returns_allowed = 'Y'        |
	--  +---------------------------------------------------+
	--
	-------------------------------
	-- CONFIGURATION SECTION ENDS
	-------------------------------
	--
	order by DrawYear, DrawMonth, Classification_Name, Classification, DrawDay, t.dh_Carrier_s, t.ROUTE_S
--) table1
--group by DrawYear, DrawMonth, Classification 
--order by DrawYear, DrawMonth, Classification
;



--This SQL shows the day boundaries to get the "week three weeks ago"
--SELECT 
--	sysdate as today1
--	, NEXT_DAY( TRUNC(sysdate), 'MONDAY')-7-(3*7)  as three
--	, NEXT_DAY( TRUNC(sysdate), 'SUNDAY')-7-(2*7)  as two
--from DUAL
----------------------










